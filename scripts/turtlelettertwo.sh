 #!/usr/bin/bash

rosservice call /turtle1/set_pen 200 50 100 5 true
rosservice call /turtle2/set_pen 50 200 100 5 true
rosservice call /turtle1/teleport_absolute 4 4 0
rosservice call /turtle2/teleport_absolute 7 7 0
rosservice call /turtle1/set_pen 200 50 100 5 false
rosservice call /turtle2/set_pen 50 200 100 5 false

rostopic  pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 3.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic  pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[-6.5, 5.0, 0.0]' '[0.0, 0.0, 4.5]'

rostopic  pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic  pub -1 /turtle2/cmd_vel geometry_msgs/Twist \ 
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.0]'
